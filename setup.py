# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in abcd/__init__.py
from abcd import __version__ as version

setup(
	name='abcd',
	version=version,
	description='cd',
	author='dc',
	author_email='dc',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
